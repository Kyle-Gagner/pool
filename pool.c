#include <stdlib.h>
#include <gtk/gtk.h>
#include <complex.h>
#include <lapacke.h>
#include <pthread.h>
#include <math.h>

#define GGEV_MAT_SIZE 160000
#define GGEV_VEC_SIZE 400

typedef struct
{
    double A[GGEV_MAT_SIZE];
    double B[GGEV_MAT_SIZE];
    double alphar[GGEV_VEC_SIZE];
    double alphai[GGEV_VEC_SIZE];
    double beta[GGEV_VEC_SIZE];
    double vr[GGEV_MAT_SIZE];
    int indices[GGEV_VEC_SIZE];
    int count;
} solution;

typedef struct
{
    GtkAdjustment *adjustment_width;
    GtkAdjustment *adjustment_depth;
    GtkAdjustment *adjustment_cparam;
    GtkAdjustment *adjustment_vparam;
    GtkAdjustment *adjustment_kernel;
    GtkAdjustment *adjustment_sigma;
    GtkAdjustment *adjustment_mode;
    solution sln;
} app_session;

int solve(app_session *session)
{
    int width = (int)gtk_adjustment_get_value(session->adjustment_width);
    int depth = (int)gtk_adjustment_get_value(session->adjustment_depth);
    double c = gtk_adjustment_get_value(session->adjustment_cparam);
    double v = gtk_adjustment_get_value(session->adjustment_vparam);
    int kernel = (int)gtk_adjustment_get_value(session->adjustment_kernel);
    double sigma = gtk_adjustment_get_value(session->adjustment_sigma);

    solution *sln = &session->sln;

    int D = width * depth;
    int N = 2 * D;
    double *A = sln->A;
    double *B = sln->B;
    for (int j = 0; j < N; j++)
    {
        for (int i = 0; i < N; i++)
        {
            A[N * j + i] = 0;
            B[N * j + i] = 0;
        }
    }
    for (int n = 0; n < D; n++)
    {
        A[N * (n + D) + n + D] = 1;
        B[N * n + n] = v;
        B[N * (n + D) + n] = c;
        B[N * n + n + D] = 1;
    }
    for (int c = 0; c < width; c++)
    {
        for (int r = 0; r < depth; r++)
        {
            int n = width * r + c;
            for (int c_ = 0; c_ < width; c_++)
            {
                for (int r_ = 0; r_ < depth; r_++)
                {
                    if (c_ == c && r_ == r) continue;
                    int x = (c_ < c ? c - c_ : c_ - c) + (r_ < r ? r - r_ : r_ - r);
                    if (x > kernel) continue;
                    int m = width * r_ + c_;
                    double y = x * exp(-(x * x) / (2 * sigma * sigma)) / (sqrt(2 * G_PI) * sigma * sigma * sigma);
                    A[N * m + n] = y;
                    A[N * n + n] -= y;
                }
            }
            /*
            int n = width * r + c;
            if (r > 0)
            {
                int m = n - width;
                A[N * n + n] -= 1;
                A[N * m + n] = 1;
            }
            if (r < depth - 1)
            {
                int m = n + width;
                A[N * n + n] -= 1;
                A[N * m + n] = 1;
            }
            if (c > 0)
            {
                int m = n - 1;
                A[N * n + n] -= 1;
                A[N * m + n] = 1;
            }
            if (c < width - 1)
            {
                int m = n + 1;
                A[N * n + n] -= 1;
                A[N * m + n] = 1;
            }
            */
        }
    }
    LAPACKE_dggev(LAPACK_COL_MAJOR, 'N', 'V', N, A, N, B, N, sln->alphar, sln->alphai, sln->beta, NULL, N, sln->vr, N);
    sln->count = 0;
    for (int j = 0; j < N; j++)
    {
        sln->indices[sln->count++] = j;
        int pair = sln->alphai[j] != 0;
        if (pair) j++;
        double max = 0;
        for (int i = 0; i < N; i++)
        {
            double magnitude = cabs(sln->vr[N * j + i] + sln->vr[N * (j + 1) + i] * I);
            if (magnitude > max)
                max = magnitude;
        }
        for (int i = 0; i < N; i++)
        {
            sln->vr[N * j + i] /= max;
            if (pair)
                sln->vr[N * (j + 1) + i] /= max;
        }
    }
    return sln->count;
}

void project(double x, double y, double z, double scl, double cx, double cy, double *sx, double *sy)
{
    *sx = scl * (0.707 * x - 0.707 * y) + cx;
    *sy = scl * (0.408 * x + 0.408 * y - 0.816 * z) + cy;
}

void interpolate_color(GdkRGBA *color1, GdkRGBA *color2, double weight1, GdkRGBA *interpolated)
{
    double weight2 = 1 - weight1;
    interpolated->red = color1->red * weight1 + color2->red * weight2;
    interpolated->green = color1->green * weight1 + color2->green * weight2;
    interpolated->blue = color1->blue * weight1 + color2->blue * weight2;
    interpolated->alpha = 1;
}

int frame = 0;

gboolean drawing_area_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
    app_session *session = (app_session*)user_data;
    solution *sln = &session->sln;
    int pad = 20;
    int width = gtk_widget_get_allocated_width (widget);
    int height = gtk_widget_get_allocated_height (widget);
    int pool_width = (int)gtk_adjustment_get_value(session->adjustment_width);
    int pool_depth = (int)gtk_adjustment_get_value(session->adjustment_depth);
    int mode = (int)gtk_adjustment_get_value(session->adjustment_mode);
    double sx, sy, cx, cy;
    double swidth = 0;
    double sheight = 0;
    project(pool_width + 1, 0, 0, 1, 0, 0, &sx, &sy);
    swidth += sx;
    cx -= sx;
    project(0, pool_depth + 1, 0, 1, 0, 0, &sx, &sy);
    swidth -= sx;
    cx -= sx;
    project(pool_width + 1, pool_depth + 1, 0, 1, 0, 0, &sx, &sy);
    sheight += sy;
    cy -= sy;
    project(0, 0, 3.5, 1, 0, 0, &sx, &sy);
    sheight -= sy;
    cy -= sy;
    double scl = (height - 2 * pad) / sheight;
    double tmp = (width - 2 * pad) / swidth;
    if (tmp < scl)
        scl = tmp;
    cx *= scl / 2;
    cy *= scl / 2;
    cx += width / 2.0;
    cy += height / 2.0;
    GdkRGBA fgcolor, bgcolor, shade1, shade2, shade3;
    GtkStyleContext *context;
    context = gtk_widget_get_style_context (widget);
    gtk_style_context_get_color(context, gtk_style_context_get_state(context), &fgcolor);
    gtk_style_context_get_background_color(context, gtk_style_context_get_state(context), &bgcolor);
    interpolate_color(&fgcolor, &bgcolor, 1.0, &shade1);
    interpolate_color(&fgcolor, &bgcolor, 0.9, &shade2);
    interpolate_color(&fgcolor, &bgcolor, 0.8, &shade3);
    gtk_render_background(context, cr, 0, 0, width, height);
    for (int p = 0; p < pool_width + pool_depth - 1; p++)
    {
        for (int r = p < pool_width ? 0 : p - pool_width + 1; p < pool_depth ? r <= p : r < pool_depth; r++)
        {
            int c = p - r;
            int n = pool_width * r + c;
            int i = sln->indices[mode - 1];
            double complex m = sln->vr[(pool_width * pool_depth * 2) * i + n];
            if (sln->alphai[i] != 0)
                m += sln->vr[(pool_width * pool_depth * 2) * (i + 1) + n] * I;
            m *= cexp((sln->alphar[i]+sln->alphai[i]*I)*(6.28 * frame / 50.0)/sln->beta[i]);
            double z = 2.0 + 1.5 * creal(m);
            double bottomx, bottomy, middlex, middley, midleftx, midlefty, midrightx, midrighty;
            project(c + 1, r + 1, 0, scl, cx, cy, &bottomx, &bottomy);
            cairo_move_to(cr, bottomx, bottomy);
            project(c + 1, r + 1, z, scl, cx, cy, &middlex, &middley);
            cairo_line_to(cr, middlex, middley);
            project(c, r + 1, z, scl, cx, cy, &midleftx, &midlefty);
            cairo_line_to(cr, midleftx, midlefty);
            project(c, r + 1, 0, scl, cx, cy, &sx, &sy);
            cairo_line_to(cr, sx, sy);
            cairo_close_path(cr);
            gdk_cairo_set_source_rgba(cr, &shade1);
            cairo_fill(cr);
            cairo_stroke(cr);
            cairo_move_to(cr, bottomx, bottomy);
            cairo_line_to(cr, middlex, middley);
            project(c + 1, r, z, scl, cx, cy, &midrightx, &midrighty);
            cairo_line_to(cr, midrightx, midrighty);
            project(c + 1, r, 0, scl, cx, cy, &sx, &sy);
            cairo_line_to(cr, sx, sy);
            cairo_close_path(cr);
            gdk_cairo_set_source_rgba(cr, &shade3);
            cairo_fill(cr);
            cairo_stroke(cr);
            project(c, r, z, scl, cx, cy, &sx, &sy);
            cairo_move_to(cr, sx, sy);
            cairo_line_to(cr, midleftx, midlefty);
            cairo_line_to(cr, middlex, middley);
            cairo_line_to(cr, midrightx, midrighty);
            cairo_close_path(cr);
            gdk_cairo_set_source_rgba(cr, &shade2);
            cairo_fill(cr);
            cairo_stroke(cr);
        }
    }
    frame++;
    return FALSE;
}

void dimensions_changed(GtkSpinButton *spin_button, gpointer user_data)
{
    app_session *session = (app_session*)user_data;
    int width = (int)gtk_adjustment_get_value(session->adjustment_width);
    int depth = (int)gtk_adjustment_get_value(session->adjustment_depth);
    int upper = solve(session);
    gtk_adjustment_set_upper(session->adjustment_mode, upper);
    double mode = gtk_adjustment_get_value(session->adjustment_mode);
    if (mode > upper)
        gtk_adjustment_set_value(session->adjustment_mode, upper);
}

void params_changed(GtkSpinButton *spin_button, gpointer user_data)
{
    app_session *session = (app_session*)user_data;
    solve(session);
}

void restart_button_clicked(GtkButton *button, gpointer user_data)
{
    frame = 0;
}

gboolean queue_draw(gpointer user_data)
{
  gtk_widget_queue_draw(GTK_WIDGET(user_data));
  return TRUE;
}

int main(int argc,char *argv[])
{
    app_session *session = malloc(sizeof(app_session));
    gtk_init(&argc, &argv);
    GtkBuilder *builder = gtk_builder_new_from_resource("/us/gagner/pool/window.ui");
    session->adjustment_width = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_width"));
    session->adjustment_depth = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_depth"));
    session->adjustment_cparam = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_cparam"));
    session->adjustment_vparam = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_vparam"));
    session->adjustment_kernel = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_kernel"));
    session->adjustment_sigma = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_sigma"));
    session->adjustment_mode = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment_mode"));
    dimensions_changed(NULL, session);
    gtk_builder_connect_signals(builder, session);
    g_timeout_add(50, queue_draw, gtk_builder_get_object(builder, "window")); 
    gtk_main();
    free(session);
    return 0;
}