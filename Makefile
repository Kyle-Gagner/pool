CFLAGS=`pkg-config --cflags gtk+-3.0 lapacke`
LFLAGS=`pkg-config --libs gtk+-3.0 gmodule-export-2.0 lapacke` -lm
run: pool
	./pool

pool: resources.c pool.c
	gcc $(CFLAGS) -o pool pool.c resources.c $(LFLAGS)

resources.c: pool.gresource.xml window.ui
	glib-compile-resources pool.gresource.xml --target=resources.c --generate-source

clean:
	rm -f pool resources.c